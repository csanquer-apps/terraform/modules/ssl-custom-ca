## Certificates
module "certs" {
  for_each = var.certs
  source   = "git::https://gitlab.com/csanquer-apps/terraform/modules/local-signed-ssl.git"

  output_path  = "${var.output_path}/${var.output_name}/certificates/${each.key}"
  output_name  = "cert_${each.key}_${var.output_name}"
  output_files = var.output_files

  algorithm       = var.algorithm
  ecdsa_curve     = var.ecdsa_curve
  rsa_bits        = var.rsa_bits
  private_key_pem = lookup(each.value, "private_key_pem", "")

  ca_algorithm       = module.ca_ints[each.value.ca].algorithm
  ca_private_key_pem = module.ca_ints[each.value.ca].private_key_pem
  ca_cert_pem        = module.ca_ints[each.value.ca].cert_pem
  ca_cert_bundle_pem = module.ca_ints[each.value.ca].cert_bundle_pem

  is_ca = false

  allowed_uses = distinct(concat([
    "key_encipherment",
    "digital_signature",
    ],
  lookup(each.value, "allowed_uses", [])))

  dns_names             = lookup(each.value, "dns_names", [])
  ip_addresses          = lookup(each.value, "ip_addresses", [])
  uris                  = lookup(each.value, "uris", [])
  validity_period_hours = lookup(each.value, "validity_period_hours", 17520)
  early_renewal_hours   = lookup(each.value, "early_renewal_hours", 0)
  set_subject_key_id    = lookup(each.value, "set_subject_key_id", false)
  common_name           = each.value.common_name
  organization          = lookup(each.value, "organization", var.organization)
  organizational_unit   = lookup(each.value, "organizational_unit", var.organizational_unit)
  street_addresses      = lookup(each.value, "street_addresses", var.street_addresses)
  locality              = lookup(each.value, "locality", var.locality)
  province              = lookup(each.value, "province", var.province)
  country               = lookup(each.value, "country", var.country)
  postal_code           = lookup(each.value, "postal_code", var.postal_code)
  serial_number         = lookup(each.value, "serial_number", "")
}
