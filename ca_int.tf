## Intermediate Certification Authorities

module "ca_ints" {
  for_each = var.ca_ints_certs
  source   = "git::https://gitlab.com/csanquer-apps/terraform/modules/local-signed-ssl.git"

  output_path  = "${var.output_path}/${var.output_name}/ca_intermediates/${each.key}"
  output_name  = "ca_int_${each.key}_${var.output_name}"
  output_files = var.output_files

  algorithm       = var.algorithm
  ecdsa_curve     = var.ecdsa_curve
  rsa_bits        = var.rsa_bits
  private_key_pem = lookup(each.value, "private_key_pem", "")

  ca_algorithm       = lookup(each.value, "ca_algorithm", (var.external_ca_root_private_key_pem == "" || var.external_ca_root_cert_pem == "" ? module.ca_root[0].algorithm : var.external_ca_root_algorithm))
  ca_private_key_pem = lookup(each.value, "ca_private_key_pem", (var.external_ca_root_private_key_pem == "" || var.external_ca_root_cert_pem == "" ? module.ca_root[0].private_key_pem : var.external_ca_root_private_key_pem))
  ca_cert_pem        = lookup(each.value, "ca_cert_pem", (var.external_ca_root_private_key_pem == "" || var.external_ca_root_cert_pem == "" ? module.ca_root[0].cert_pem : var.external_ca_root_cert_pem))

  is_ca = true

  allowed_uses = distinct(concat([
    "cert_signing",
    "key_encipherment",
    "digital_signature",
    ],
  lookup(each.value, "allowed_uses", [])))

  dns_names             = lookup(each.value, "dns_names", [])
  ip_addresses          = lookup(each.value, "ip_addresses", [])
  uris                  = lookup(each.value, "uris", [])
  validity_period_hours = lookup(each.value, "validity_period_hours", 17520)
  early_renewal_hours   = lookup(each.value, "early_renewal_hours", 0)
  set_subject_key_id    = lookup(each.value, "set_subject_key_id", false)
  common_name           = each.value.common_name
  organization          = lookup(each.value, "organization", var.organization)
  organizational_unit   = lookup(each.value, "organizational_unit", var.organizational_unit)
  street_addresses      = lookup(each.value, "street_addresses", var.street_addresses)
  locality              = lookup(each.value, "locality", var.locality)
  province              = lookup(each.value, "province", var.province)
  country               = lookup(each.value, "country", var.country)
  postal_code           = lookup(each.value, "postal_code", var.postal_code)
  serial_number         = lookup(each.value, "serial_number", "")
}
