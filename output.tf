## Root Certification Authority

output "ca_root_infos" {
  value = var.external_ca_root_private_key_pem == "" || var.external_ca_root_cert_pem == "" ? module.ca_root[0].cert_infos : map()
}

output "ca_root_contents" {
  sensitive = true
  value     = var.external_ca_root_private_key_pem == "" || var.external_ca_root_cert_pem == "" ? module.ca_root[0].cert_contents : map()
}

output "ca_root_paths" {
  value = var.external_ca_root_private_key_pem == "" || var.external_ca_root_cert_pem == "" ? module.ca_root[0].cert_paths : map()
}

## Intermediate Certification Authorities

output "ca_ints_infos" {
  value = { for k, v in module.ca_ints : k => v.cert_infos }
}

output "ca_ints_contents" {
  sensitive = true
  value     = { for k, v in module.ca_ints : k => v.cert_contents }
}

output "ca_ints_paths" {
  value = { for k, v in module.ca_ints : k => v.cert_paths }
}

## Certificates

output "certs_infos" {
  value = { for k, v in module.certs : k => v.cert_infos }
}

output "certs_contents" {
  sensitive = true
  value     = { for k, v in module.certs : k => v.cert_contents }
}

output "certs_paths" {
  value = { for k, v in module.certs : k => v.cert_paths }
}
