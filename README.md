terraform-module-custom-ca-ssl
==============================


[![pipeline status](https://gitlab.com/csanquer-apps/terraform/modules/ssl-custom-ca/badges/master/pipeline.svg)](https://gitlab.com/csanquer-apps/terraform/modules/ssl-custom-ca/-/commits/master)

Terraform module to create signed SSL certificates with custom Root and Intermediate Certification Authorities


## How To

call your module in your terraform file : 

```terraform

module "my_custom_ssl_chain" {
    source = "git::ssh://git@gitlab.com:csanquer-apps/terraform/modules/ssl-custom-ca.git?ref=master"

    # control how to output files ( to ssl directory with my_custom prefix name)
    output_path  = "ssl"
    output_name  = "my_custom"
    output_files = true

    # default SSL subject informations for all certificates
    organization  = "My Organization"
    organizational_unit = "My Organizational Unit"
    street_addresses = []
    locality = "San Francisco"
    province = "California"
    country = "US"
    postal_code = "94133"

    # Root CA subject common name
    ca_root_common_name = "ca-root.example.org"

    # Intermediate Certification authorities
    ca_ints_certs = {
        "int1" = {
            common_name = "ca-int1.example.org"
        }
    }

    # certificates
    certs = {
        "website1" = {
            common_name = "website1.example.org"
            ca = "int1"
        }
    }
}

## Root Certification Authority

# Root CA properties
output "ca_root_infos" {
  value = module.my_custom_ssl_chain.ca_root_infos
}

# Root CA key and certificates contents
output "ca_root_contents" {
  sensitive = true
  value     = module.my_custom_ssl_chain.ca_root_contents
}

# Root CA key and certificates file paths (if output)
output "ca_root_paths" {
  value = module.my_custom_ssl_chain.ca_root_paths
}

## Intermediate Certification Authorities

# Intermediate CA properties
output "ca_ints_infos" {
  value = module.my_custom_ssl_chain.ca_ints_infos
}

# Intermediate CA key and certificates contents
output "ca_ints_contents" {
  sensitive = true
  value     = module.my_custom_ssl_chain.ca_ints_contents
}

# Intermediate CA key and certificates file paths (if output)
output "ca_ints_paths" {
  value = module.my_custom_ssl_chain.ca_ints_paths
}

## Certificates

# Certificates CA properties
output "certs_infos" {
  value = module.my_custom_ssl_chain.certs_infos
}

# keys and certificates contents
output "certs_contents" {
  sensitive = true
  value     = module.my_custom_ssl_chain.certs_contents
}

# keys and certificates file paths (if output)
output "certs_paths" {
  value = module.my_custom_ssl_chain.certs_paths
}


```
