variable "algorithm" {
  description = "SSL algorithm (RSA or ECDSA)."
  type        = string
  default     = "RSA"
}

variable "ecdsa_curve" {
  type        = string
  description = "ECDSA Elliptic curve (P224, P256, P384 or P521)."
  default     = "P256"
}

variable "rsa_bits" {
  description = "RSA key size in bits."
  type        = number
  default     = 2048
}

## output files
variable "output_path" {
  description = "Output directory path for all certificates files"
  type        = string
  default     = "./ssl"
}

variable "output_name" {
  description = "Output name"
  type        = string
  default     = "my_custom"
}

variable "output_files" {
  description = "Output to files"
  type        = bool
  default     = false
}

## SSL default subject

variable "organization" {
  description = "Default SSL subject organization name."
  type        = string
  default     = ""
}

variable "organizational_unit" {
  description = "Default SSL subject organizational unit name."
  type        = string
  default     = ""
}

variable "street_addresses" {
  description = "Default SSL subject street addresses list."
  type        = list(string)
  default     = []
}

variable "locality" {
  description = "Default SSL subject locality."
  type        = string
  default     = ""
}

variable "province" {
  description = "Default SSL subject province."
  type        = string
  default     = ""
}

variable "country" {
  description = "Default SSL subject country."
  type        = string
  default     = ""
}

variable "postal_code" {
  description = "Default SSL subject postal code."
  type        = string
  default     = ""
}

## Root Certification Authority

variable "external_ca_root_algorithm" {
  description = "External SSL algorithm (RSA or ECDSA) from Root Certificate Authority."
  type        = string
  default     = "RSA"
}

variable "external_ca_root_private_key_pem" {
  description = "External private key provided in PEM encoded format from Root Certificate Authority."
  type        = string
  default     = ""
}

variable "external_ca_root_cert_pem" {
  description = "External Root Certificate Authority provided in PEM encoded format."
  type        = string
  default     = ""
}


variable "ca_root_validity_period_hours" {
  description = "The number of hours after initial issuing that the certificate will become invalid."
  type        = number
  default     = 17520 # 2×365×24 = 17520 = 2 years
}

variable "ca_root_allowed_uses" {
  description = "List of keywords from RFC5280 describing a use that is permitted. For more info and the list of keywords, see https://www.terraform.io/docs/providers/tls/r/self_signed_cert.html#allowed_uses."
  type        = list(string)
  default     = []
}

variable "ca_root_dns_names" {
  description = "List of DNS names for which a certificate is being requested."
  type        = list(string)
  default     = []
}

variable "ca_root_ip_addresses" {
  description = "List of IP addresses for which a certificate is being requested."
  type        = list(string)
  default     = []
}

variable "ca_root_uris" {
  description = "List of URIs for which a certificate is being requested."
  type        = list(string)
  default     = []
}

variable "ca_root_early_renewal_hours" {
  description = "If set, the resource will consider the certificate to have expired the given number of hours before its actual expiry time."
  type        = number
  default     = 0
}

variable "ca_root_set_subject_key_id" {
  description = "If true, the certificate will include the subject key identifier. Defaults to false, in which case the subject key identifier is not set at all."
  type        = bool
  default     = false
}

variable "ca_root_common_name" {
  description = "SSL subject Common Name = Domain."
  type        = string
}

variable "ca_root_organization" {
  description = "SSL subject organization name."
  type        = string
  default     = ""
}

variable "ca_root_organizational_unit" {
  description = "SSL subject organizational unit name."
  type        = string
  default     = ""
}

variable "ca_root_street_addresses" {
  description = "SSL subject street addresses list."
  type        = list(string)
  default     = []
}

variable "ca_root_locality" {
  description = "SSL subject locality."
  type        = string
  default     = ""
}

variable "ca_root_province" {
  description = "SSL subject province."
  type        = string
  default     = ""
}

variable "ca_root_country" {
  description = "SSL subject country."
  type        = string
  default     = ""
}

variable "ca_root_postal_code" {
  description = "SSL subject postal code."
  type        = string
  default     = ""
}

variable "ca_root_serial_number" {
  description = "SSL subject serial number."
  type        = string
  default     = ""
}

## Intermediate Certification Authorities

variable "ca_ints_certs" {
  description = "Intermediate Certification Authorities to create : {name => options}."
  default = {
    # "main" = {
    #   common_name = "" # required
    #   private_key_pem = ""
    #   ca_algorithm = ""
    #   ca_private_key_pem = ""
    #   ca_cert_pem = ""
    #   allowed_uses = ""
    #   dns_names = []
    #   ip_addresses = []
    #   uris = []
    #   validity_period_hours = 17520
    #   early_renewal_hours = 0
    #   set_subject_key_id = false
    #   organization          = ""
    #   organizational_unit   = ""
    #   street_addresses      = []
    #   locality              = ""
    #   province              = ""
    #   country               = ""
    #   postal_code           = ""
    #   serial_number         = ""
    # }
  }
}

## certificates
variable "certs" {
  description = "Certificates to create : {name => options}."
  default = {
    # "main" = {
    #   common_name = "" # required
    #   ca = "" # ca intermediate key name, required,  see var.ca_ints_certs map keys
    #   private_key_pem = ""
    #   allowed_uses = ""
    #   dns_names = []
    #   ip_addresses = []
    #   uris = []
    #   validity_period_hours = 17520
    #   early_renewal_hours = 0
    #   set_subject_key_id = false
    #   organization          = ""
    #   organizational_unit   = ""
    #   street_addresses      = []
    #   locality              = ""
    #   province              = ""
    #   country               = ""
    #   postal_code           = ""
    #   serial_number         = ""
    # }
  }
}
