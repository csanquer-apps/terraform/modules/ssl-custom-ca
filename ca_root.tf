## Root Certification Authority
module "ca_root" {
  count  = var.external_ca_root_private_key_pem == "" || var.external_ca_root_cert_pem == "" ? 1 : 0
  source = "git::https://gitlab.com/csanquer-apps/terraform/modules/self-signed-ssl.git"

  output_path  = "${var.output_path}/${var.output_name}/ca_root"
  output_name  = "ca_root_${var.output_name}"
  output_files = var.output_files

  algorithm       = var.algorithm
  ecdsa_curve     = var.ecdsa_curve
  rsa_bits        = var.rsa_bits
  private_key_pem = var.external_ca_root_private_key_pem

  is_ca = true

  allowed_uses = distinct(concat([
    "cert_signing",
    "key_encipherment",
    "digital_signature",
    ],
  var.ca_root_allowed_uses))

  dns_names             = var.ca_root_dns_names
  ip_addresses          = var.ca_root_ip_addresses
  uris                  = var.ca_root_uris
  validity_period_hours = var.ca_root_validity_period_hours
  early_renewal_hours   = var.ca_root_early_renewal_hours
  set_subject_key_id    = var.ca_root_set_subject_key_id

  common_name         = var.ca_root_common_name
  organization        = var.ca_root_organization == "" ? var.organization : var.ca_root_organization
  organizational_unit = var.ca_root_organizational_unit == "" ? var.organizational_unit : var.ca_root_organizational_unit
  street_addresses    = length(var.ca_root_street_addresses) == 0 ? var.street_addresses : var.ca_root_street_addresses
  locality            = var.ca_root_locality == "" ? var.locality : var.ca_root_locality
  province            = var.ca_root_province == "" ? var.province : var.ca_root_province
  country             = var.ca_root_country == "" ? var.country : var.ca_root_country
  postal_code         = var.ca_root_postal_code == "" ? var.postal_code : var.ca_root_postal_code
  serial_number       = var.ca_root_serial_number
}
